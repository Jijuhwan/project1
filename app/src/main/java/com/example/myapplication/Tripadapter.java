package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Tripadapter extends BaseAdapter {
    Context context;
    int layout;
    public ArrayList<Board> data ;
    LayoutInflater inflater;


    public Tripadapter(Context context, int layout, ArrayList data) {
        this.context = context;
        this.layout = layout;
        this.data = data;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {     //각 아이템에 들어갈 데이터들의 전체 개수를 리턴하는 메소드.
        return data.size();
    }

    @Override
    public Object getItem(int position) {       //아이템의 위치에 해당하는 데이터의 객체를 반환하는 메소드
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {       //아이템의 위치에 해당하는 id를 반환하는 메소드입니다.(보통 리턴 값으로 자기의 위치를 그대로 반환)
        return position;
    }

    @Override
    public int getItemViewType(int position) {      //각 위치의 데이터에 해당하는 레이아웃 타입을 반환.
        return data.get(position).gettype();
    }

    @Override
    public int getViewTypeCount() {   //레이아웃의 최댓값이 들어감.
        return 2;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {     //아이템의 레이아웃을 구현하고 리스트뷰에 아이템을 배치하는 역활을 함. ( 데이터 갯수당 호출 됨)
        int viewType = getItemViewType(position);
        final Context context = parent.getContext();

        //레이아웃 형성 및 인프레이트
        if (convertView == null) {

            convertView = inflater.inflate(R.layout.trip, parent, false);

        }
            TextView tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
            TextView tvContent = (TextView) convertView.findViewById(R.id.tvContent);
            TextView tvDate = (TextView) convertView.findViewById(R.id.tvDate);

            Board board = data.get(position);
            tvTitle.setText(board.gettitle());
            tvContent.setText(board.getcontent());
            tvDate.setText(board.getdate());

        return convertView;
    }

    public void addItem(String tvTitle, String tvContent, String tvDate)
    {
        Board item = new Board();
        item.settitle(tvTitle);
        item.setcontent(tvContent);
        item.setdate(tvDate);

        data.add(item);
    }

    public void clearItem(){
        data.clear();
    }
}
