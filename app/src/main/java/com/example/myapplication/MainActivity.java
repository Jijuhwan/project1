package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.solver.widgets.Snapshot;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class MainActivity extends AppCompatActivity {
ListView lv;
Tripadapter trip;
ArrayList board1;
Button btn1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lv = (ListView) findViewById(R.id.lv);
        btn1 = (Button) findViewById(R.id.btn1);

        board1 = new ArrayList<Board>();
        trip = new Tripadapter(this,R.layout.trip,board1);

        board1.add(new Board("", "", "", 0));
        lv.setAdapter(trip);

        DatabaseReference database = FirebaseDatabase.getInstance().getReference();
        database.child("message").addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Board board = dataSnapshot.getValue(Board.class);
                board1.add(board);
                lv.setAdapter(trip);
                trip.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });


        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText edit = (EditText) findViewById(R.id.edit);
                EditText edit2 = (EditText) findViewById(R.id.edit2);

                SimpleDateFormat dateform = new SimpleDateFormat("yyyy-MM-dd");
                Date date = new Date();

                DatabaseReference database = FirebaseDatabase.getInstance().getReference();
                Board board = new Board(edit.getText().toString(), dateform.format(date).toString(), edit2.getText().toString(), 1);

                database.child("message").push().setValue(board);
                edit.setText("");
                edit2.setText("");

                Toast.makeText(getApplicationContext(), "게시글이 등록되었습니다.", Toast.LENGTH_SHORT).show();
            }

        });
    }

}
